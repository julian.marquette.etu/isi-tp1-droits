# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Marquette, Julian, email: julian.marquette.etu@univ-lille.fr

- Liard, Thomas, email: thomas.liard.etu@univ-lille.fr

## Question 1

Si on est connecté avec l'utilisateur toto :
Ca ne fonctionne pas car il voit que l'owner == l'utilisateur -> il voit que l'owner est en read only -> donc il n'autorise pas l'écriture dans le fichier et ça ne fonctionne pas.

Si on est connecté avec l'utilisateur ubuntu :
On peut écrire dans le fichier car le processus regarde qui est l'utilisateur et le compare avec l'owner de titi.txt. Il voit que ubuntu != toto. Il regarde ensuite les droits du groupe affecté au fichier. Ici, on est en read et write sur le groupe ubuntu -> Comme l'utilisateur ubuntu est dans le groupe ubuntu, il est autorisé à écrire dans le fichier -> Donc ça marche.

## Question 2

- Le caractère x pour un répertoire spécifie si il est accessible (ou non) pour l'utilisateur/groupe/autres

- En essayant d'y accéder avec toto, on obtient :
```txt
bash: cd: mydir/: Permission non accordée
```
-> Parce que le x n'est pas actif pour le groupe ubuntu, et l'owner du directory n'est pas toto.

- En essayant ls -all mydir avec toto :
```txt
ls: impossible d'accéder à 'mydir/data.txt': Permission non accordée
ls: impossible d'accéder à 'mydir/.': Permission non accordée
ls: impossible d'accéder à 'mydir/..': Permission non accordée
total 0
d????????? ? ? ? ?              ? .
d????????? ? ? ? ?              ? ..
-????????? ? ? ? ?              ? data.txt
```
-> Le droit r sur le groupe ubuntu permet à toto de lister le contenu de mydir, même si il ne peut pas accéder aux fichiers.



## Question 3

En lançant le programme avec toto (1001 = user id de l'user toto, 1002 = id du groupe ubuntu) :
```txt
Effective ->
  EUID     : 1001
  EGID     : 1002
Real      ->
  RUID     : 1001
  RGID     : 1002
Cannot open file: Permission denied
```
Le processus n'arrive pas à ouvrir le fichier en lecture.

Après avoir set le flag sur l'utilisateur ubuntu (utilistaeur ubuntu id = 1000) :
```txt
Effective ->
  EUID     : 1000
  EGID     : 1002
Real      ->
  RUID     : 1001
  RGID     : 1002
File opens correctly
```
Le processus arrive à ouvrir le fichier et on voit qu'il simule le lancement par l'utilisateur ubuntu (EUID = 1000), alors qu'en réalité il s'est lancé depuis l'utilisateur toto (RUID 1001)


## Question 4

Lorsque l'on essaie d'éxecuter le script suid.py avec l'utilisateur toto : 
  EUID     : 1001
  EGID     : 1001
  Cannot open file

Car c'est l'interpréteur de python qui éxecute le code.

Pour pouvoir faire en sorte que toto exécute suid.py correctement,
il faut mettre "sudo chmod u+s /usr/bin/python3.8" 

Et donc le retour du script suid.py est : 
  EUID     : 0
  EGID     : 1001
File /home/ubuntu/mydir/mydata.txt opens correctly

## Question 5

La commande chfn permet de mofidier les informations d'un utilisateur -> son nom, numéro de bureau, téléphone ...

résultat de ls -al /usr/bin/chfn :
```txt
-rwsr-xr-x 1 root root 76496 mars  22  2019 /usr/bin/chfn
```

dans etc/passwd on trouve pour toto :
```txt
toto:x:1001:1002:toto,toto bureau,toto phone pro,toto phone perso:/home/toto:/bin/bash
```
Les informations ont bien été transmises.

## Question 6

Les mots de passes sont stockés dans le fichier etc/shadow. Ils ne sont pas en clair dedans, on voit leur hashage, leur algorithme
de hashage et leur salt. Cela permet plus de sécurité pour que personne, même un admin ne puisse avoir accés au mot de passe des utilisateurs en dur. De plus les droits requis pour ne serais-ce que lire le fichier shadow forcent à être un admin.

## Question 7

Tout fonctionne comme le demande l'énoncé, sauf une chose :

Je n'arrive pas à faire en sorte qu'un utilisateur a1 puisse modifier un fichier dont le owner est a2, alors qu'il devrait pouvoir le faire. Un touch toto.txt donne les droit suivant au fichier toto : rw-r--r-- -> et donc un autre utilisateur du même groupe n'a pas le droit write (w). Je ne vois pas comment override ce comportement lors d'un touch ou mkdir.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








