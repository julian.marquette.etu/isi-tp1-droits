#!/bin/bash

# Créer les 3 folders
sudo mkdir dir_a
sudo mkdir dir_b
sudo mkdir dir_c

# Fonction qui créer un fichier, met du texte puis set son owner:group
# $1 = chemin du fichier, $2 = user:groupe, $3 = texte du fichier
function createFile () {
	sudo touch $1
	sudo sh -c "echo "$3" > $1"
	sudo chown $2 $1
}

# Créer des fichiers dans les folders avec des owners spécifiques
# dir_a :
createFile "dir_a/data1" "a1:groupe_a" "a1_file_data"
createFile "dir_a/data2" "a2:groupe_a" "a2_file_data"
# dir_b :
createFile "dir_b/data1" "b1:groupe_b" "b1_file_data"
createFile "dir_b/data2" "b2:groupe_b" "b2_file_data"
# dir_c :
createFile "dir_c/admindata" "admin:admin" "admin_data_!"

# Décrit les owners + groupes des folders
sudo chown admin:groupe_a dir_a
sudo chown admin:groupe_b dir_b
sudo chown admin:admin dir_c

# Créer les permissions des folders
sudo chmod 770 dir_a/
sudo chmod +t dir_a/
sudo chmod g+s dir_a/
sudo chmod 770 dir_b/
sudo chmod +t dir_b/
sudo chmod g+s dir_b/
sudo chmod 775 dir_c
sudo chmod +t dir_c
sudo chmod g+s dir_c/


# Créer un user :
# sudo adduser a1
# sudo usermod -g groupe_a a1

