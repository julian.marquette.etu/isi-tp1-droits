#define _POSIX_SOURCE
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>


int main(int argc, char *argv[]) {

    // Demande le mot de passe de l'utilisateur
    char password[255];
    printf("Enter your password: ");
    fgets(password, 10, stdin);
    printf("MDP : %s", password);

	// Récupération des ids réels de l'utilisateur
	uid_t ruid = getuid();
	uid_t rgid = getgid();

    puts("Real      ->");
	printf("  RUID     : %d\n", (int) ruid);
	printf("  RGID     : %d\n", (int) rgid);


    // Récupération de l'owner id et du group id du fichier ciblé
    struct stat fileStat;
    struct passwd pwent;  
    struct passwd *pwentp;
    char buf[_SC_GETPW_R_SIZE_MAX];

    if(stat("./rmg.c", &fileStat) < 0) {
        perror("Le fichier est innaccessible !"); 
    }

    printf("%7d ", fileStat.st_uid);
    printf("%7d ", fileStat.st_gid);

    // Récupération des droits du fichier
    printf("\nFile Permissions: \n");
    printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
    printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
    printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
    printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
    printf("\n");
	

  	return 0;
}