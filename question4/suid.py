import os 
import sys
euid = os.geteuid()
egid = os.getegid()
print("EUID = " + str(euid))
print("EGID = " + str(egid))

if len(sys.argv) != 2 : 
    print("Missing argument\n")
    exit()

filename = sys.argv[1]

try:
    with open(filename,"r") as f:
        print("File " + filename + " opens correctly\n")
except IOError:
        print("Cannot open file")
        exit()