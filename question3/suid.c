#define _POSIX_SOURCE
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	// ############# Récupération des ids #############
	uid_t  euid = geteuid();
	uid_t  egid = getegid();
	uid_t  ruid = getuid();
	uid_t  rgid = getgid();
	  
	puts("Effective ->");
	printf("  EUID     : %d\n", (int) euid);
	printf("  EGID     : %d\n", (int) egid);
	puts("Real      ->");
	printf("  RUID     : %d\n", (int) ruid);
	printf("  RGID     : %d\n", (int) rgid);

	// ############# Lecture d'un fichier #############

	FILE *f;
	if (argc < 2) {
		printf("Missing argument\n");
		exit(EXIT_FAILURE);
	}

	f = fopen(argv[1], "r");
	if (f == NULL) {
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}

	printf("File opens correctly\n");
	fclose(f);
	exit(EXIT_SUCCESS);

  	return 0;
}
